package de.foodsharing.api

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import org.threeten.bp.ZonedDateTime
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date

/**
 * Responsible for parsing different date formats that can occur in the JSON responses.
 */
class DateDeserializer : JsonDeserializer<Date> {

    private val formatters =
            listOf("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss").map { SimpleDateFormat(it) }

    @Throws(JsonParseException::class)
    override fun deserialize(
        jsonElement: JsonElement,
        typeOF: Type,
        context: JsonDeserializationContext
    ): Date {
        try {
            val dateTime = ZonedDateTime.parse(jsonElement.asString)
            return Date(dateTime.toEpochSecond() * 1000)
        } catch (e: Throwable) {}

        // try given date formats
        for (formatter in formatters) {
            try {
                return formatter.parse(jsonElement.asString)
            } catch (e: ParseException) {}
        }

        // try to parse as timestamp (seconds)
        try {
            return Date(jsonElement.asLong * 1000)
        } catch (e: ParseException) {}

        throw JsonParseException("Failed to parse date: \"${jsonElement.asString}\"")
    }
}
