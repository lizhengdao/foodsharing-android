package de.foodsharing.api

import de.foodsharing.model.FoodSharePoint
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface FoodSharePointAPI {
    @GET("/api/foodSharePoints/{id}")
    fun get(@Path("id") id: Int): Observable<FoodSharePoint>
}
