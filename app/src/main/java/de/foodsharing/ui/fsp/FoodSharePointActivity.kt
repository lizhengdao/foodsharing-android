package de.foodsharing.ui.fsp

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.View.INVISIBLE
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import de.foodsharing.R
import de.foodsharing.api.PostsAPI
import de.foodsharing.di.Injectable
import de.foodsharing.model.FoodSharePoint
import de.foodsharing.ui.base.AuthRequiredBaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.picture.PictureActivity
import de.foodsharing.ui.picture.PictureActivity.Companion.EXTRA_PICTURE_URL
import de.foodsharing.ui.posts.PostsFragment
import de.foodsharing.utils.BASKET_MAP_ZOOM
import de.foodsharing.utils.LINK_BASE_URL
import de.foodsharing.utils.OsmdroidUtils
import de.foodsharing.utils.Utils
import de.foodsharing.utils.Utils.getFoodSharePointPhotoURL
import de.foodsharing.utils.Utils.getFspMakerIconBitmap
import kotlinx.android.synthetic.main.activity_food_share_point.toolbar
import kotlinx.android.synthetic.main.activity_food_share_point.progress_bar
import kotlinx.android.synthetic.main.activity_food_share_point.fsp_name
import kotlinx.android.synthetic.main.activity_food_share_point.fsp_address
import kotlinx.android.synthetic.main.activity_food_share_point.fsp_description
import kotlinx.android.synthetic.main.activity_food_share_point.fsp_picture
import kotlinx.android.synthetic.main.activity_food_share_point.fsp_posts_fragment
import kotlinx.android.synthetic.main.activity_food_share_point.fsp_content_view
import kotlinx.android.synthetic.main.activity_food_share_point.fsp_location_view
import javax.inject.Inject

class FoodSharePointActivity : AuthRequiredBaseActivity(), Injectable {

    companion object {
        const val EXTRA_FSP_ID = "id"

        private const val FOOD_SHARE_POINT_URL = "$LINK_BASE_URL/fairteiler/%d"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: FoodSharePointViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(FoodSharePointViewModel::class.java)
    }

    private var mapSnapshotDetachAction: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food_share_point)
        rootLayoutID = android.R.id.content
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bindViewModel()

        val id = when {
            intent.hasExtra(EXTRA_FSP_ID) -> intent.getIntExtra(EXTRA_FSP_ID, -1)
            intent.data?.pathSegments?.contains("fairteiler") == true -> {
                intent.data!!.pathSegments.last().toIntOrNull()
            }
            intent.data?.queryParameterNames?.contains("page") == true
                    && intent.data?.getQueryParameter("page").equals("fairteiler") -> {
                intent.data!!.getQueryParameter("id")?.toIntOrNull()
            }
            else -> null
        }

        if (id != null) {
            supportActionBar?.title = "${getString(R.string.foodsharepoint_label_short)} #$id"
            viewModel.foodSharePointId = id
            (fsp_posts_fragment as PostsFragment).setTarget(PostsAPI.Target.FOOD_SHARE_POINT, id)
        } else {
            showErrorMessage(getString(R.string.foodsharepoint_404))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.fsp_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.foodsharepoint_copy_button -> {
            getFoodSharePointUrl()?.let {
                Utils.copyToClipboard(this, it)
                Toast.makeText(this, getString(R.string.copied_url), Toast.LENGTH_SHORT).show()
            }
            true
        }
        R.id.foodsharepoint_share_button -> {
            getFoodSharePointUrl()?.let {
                Utils.openShareDialog(this, it)
            }
            true
        }
        R.id.foodsharepoint_open_website_button -> {
            getFoodSharePointUrl()?.let {
                if (!Utils.openUrlInBrowser(this, it))
                    showErrorMessage(getString(R.string.browser_not_found))
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun getFoodSharePointUrl() =
        viewModel.foodSharePoint.value?.let { FOOD_SHARE_POINT_URL.format(it.id) }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        mapSnapshotDetachAction?.invoke()
        super.onDestroy()
    }

    private fun bindViewModel() {
        viewModel.isLoading.observe(this, Observer {
            progress_bar.visibility = if (it) VISIBLE else INVISIBLE
        })

        viewModel.showError.observe(this, EventObserver {
            showErrorMessage(getString(it))
        })

        viewModel.foodSharePoint.observe(this, Observer {
            showFoodSharePoint(it)
        })
    }

    private fun showFoodSharePoint(fsp: FoodSharePoint) {
        fsp_name.text = fsp.name
        supportActionBar?.title = fsp.name
        fsp_address.text = "${fsp.address}\n${fsp.postcode} ${fsp.city}"
        fsp_description.text = fsp.description

        if (fsp.picture.isNullOrEmpty()) fsp_picture.visibility = GONE
        else fsp.picture.let { pictureId ->
            Picasso.get()
                .load(getFoodSharePointPhotoURL(pictureId, Utils.FoodSharePointPhotoType.HEAD))
                .fit()
                .centerCrop()
                .into(fsp_picture)

            fsp_picture.setOnClickListener {
                val intent = Intent(this, PictureActivity::class.java).apply {
                    putExtra(EXTRA_PICTURE_URL,
                        getFoodSharePointPhotoURL(pictureId, Utils.FoodSharePointPhotoType.NORMAL))
                }
                startActivity(intent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
            }
        }

        fsp_content_view.visibility = VISIBLE

        mapSnapshotDetachAction = OsmdroidUtils.loadMapTileToImageView(
            fsp_location_view, fsp.toCoordinate(), BASKET_MAP_ZOOM,
            getFspMakerIconBitmap(this.applicationContext)
        )
        fsp_location_view.setOnClickListener {
            Utils.openCoordinate(
                this,
                fsp.toCoordinate(),
                getString(R.string.foodsharepoint_label_short)
            )
        }
    }

    private fun showErrorMessage(error: String) {
        progress_bar.visibility = INVISIBLE
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }
}
