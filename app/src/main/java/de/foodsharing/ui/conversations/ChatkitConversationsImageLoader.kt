package de.foodsharing.ui.conversations

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.widget.ImageView
import de.foodsharing.utils.CachedResourceLoader
import de.foodsharing.utils.ChatkitImageLoader
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Utility class that loads multiple images from URLS and combines them into one image.
 */
class ChatkitConversationsImageLoader(
    private val defaultImage: String? = null,
    private val resourceLoader: CachedResourceLoader
) :
        ChatkitImageLoader(defaultImage, resourceLoader) {

    private val imageMargin = 4

    override fun loadImage(imageView: ImageView, url: String?, payload: Any?) {
        imageView.setImageBitmap(null)

        val urls = url?.split("|")

        if (urls != null && urls.size > 1) {
            val os = urls.map { resourceLoader.createImageObservable(it, defaultImage) }

            Observable.merge(os).toList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        imageView.setImageBitmap(
                                combineImages(it).apply {
                                    setHasAlpha(true)
                                }
                        )
                    }, {
                        // Loading the default images failed, do nothing
                    })
        } else super.loadImage(imageView, url, payload)
    }

    /**
     * Combines up to four images into one.
     */
    private fun combineImages(images: List<Bitmap>): Bitmap {
        val firstSize = Math.max(images[0].width, images[0].height)
        val result = Bitmap.createBitmap(
                firstSize * 2 + imageMargin,
                firstSize * 2 + imageMargin,
                Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(result)
        val paint = Paint()
        for (i in images.indices) {
            val source = Rect(0, 0, images[i].width - 1, images[i].height - 1)
            val left = (firstSize + imageMargin) * (i % 2)
            val top = (firstSize + imageMargin) * (i / 2)
            val offset = (Math.abs(images[i].width - images[i].height) / 2.0).toInt()
            val dest = if (images[i].width < images[i].height) {
                Rect(left + offset, top, left + firstSize - 1 - offset, top + firstSize - 1)
            } else {
                Rect(left, top + offset, left + firstSize - 1, top + firstSize - 1 - offset)
            }
            canvas.drawBitmap(images[i], source, dest, paint)
        }
        return result
    }
}
