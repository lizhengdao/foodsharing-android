package de.foodsharing.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import de.foodsharing.ui.baskets.BasketsFragment
import de.foodsharing.ui.conversations.ConversationsFragment
import de.foodsharing.ui.map.MapFragment
import de.foodsharing.ui.posts.PostsFragment

@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector
    abstract fun bindConversationListFragment(): ConversationsFragment

    @ContributesAndroidInjector
    abstract fun bindMapFragment(): MapFragment

    @ContributesAndroidInjector
    abstract fun bindBasketListFragment(): BasketsFragment

    @ContributesAndroidInjector
    abstract fun bindPostsFragment(): PostsFragment
}
