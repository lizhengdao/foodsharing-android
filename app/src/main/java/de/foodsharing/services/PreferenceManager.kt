package de.foodsharing.services

import android.content.Context
import android.content.SharedPreferences
import de.foodsharing.R
import de.foodsharing.model.Coordinate
import de.foodsharing.utils.DEFAULT_MAP_ZOOM
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.abs

@Singleton
class PreferenceManager @Inject constructor(
    private val preferences: SharedPreferences,
    private val context: Context
) {
    var isLoggedIn: Boolean
        get() = preferences.getBoolean(context.getString(R.string.preferenceLoggedIn), false)
        set(value) = preferences.edit().putBoolean(context.getString(R.string.preferenceLoggedIn), value).apply()

    var wasLastContactByMessage: Boolean
        get() = preferences.getBoolean(context.getString(R.string.preferenceLastContactByMessage), false)
        set(value) = preferences.edit().putBoolean(context.getString(R.string.preferenceLastContactByMessage), value)
            .apply()

    var wasLastContactByPhone: Boolean
        get() = preferences.getBoolean(context.getString(R.string.preferenceLastContactByMessage), false)
        set(value) = preferences.edit().putBoolean(context.getString(R.string.preferenceLastContactByPhone), value)
            .apply()

    var mapZoom: Double
        get() = preferences.getFloat(context.getString(R.string.preferenceMapZoom), DEFAULT_MAP_ZOOM.toFloat())
            .toDouble()
        set(value) = preferences.edit().putFloat(context.getString(R.string.preferenceMapZoom), value.toFloat()).apply()

    var mapCenterCoordinate: Coordinate?
        get() {
            if (!preferences.contains(context.getString(R.string.preferenceMapCenterLat))) return null
            if (!preferences.contains(context.getString(R.string.preferenceMapCenterLon))) return null
            val lat = preferences.getFloat(context.getString(R.string.preferenceMapCenterLat), 0.0f).toDouble()
            val lon = preferences.getFloat(context.getString(R.string.preferenceMapCenterLon), 0.0f).toDouble()
            // Due to a previous bug the map was sometimes centered initially at 0,0. When the user changed the camera
            // position this value was stored in the preferences. Because of that we discard all values close to
            // this coordinate.
            if (abs(lat) < 4.0 && abs(lon) < 4.0) {
                return null
            }
            return Coordinate(lat, lon)
        }
        set(value) {
            preferences.edit().let {
                if (value == null) {
                    it.remove(context.getString(R.string.preferenceMapCenterLat))
                        .remove(context.getString(R.string.preferenceMapCenterLon))
                } else {
                    it.putFloat(context.getString(R.string.preferenceMapCenterLat), value.lat.toFloat())
                        .putFloat(context.getString(R.string.preferenceMapCenterLon), value.lon.toFloat())
                }
            }.apply()
        }
}
